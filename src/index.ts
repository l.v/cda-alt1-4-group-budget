import { PrismaClient } from '@prisma/client'
import * as express from 'express';

import buildUsersRouter from './modules/users/infrastructure/router';
import buildSGroupRouter from './modules/spendingGroup/infrastructure/router';


const prisma = new PrismaClient();



const usersRouter = buildUsersRouter(prisma.user, prisma.group);
const sGroupRouter = buildSGroupRouter(prisma.user, prisma.group);


const app = express();
app.use(express.json());
app.use('/user', usersRouter);
app.use('/group', sGroupRouter);

app.listen(3000, () => {
    console.log(`Example app listening at http://localhost:3000`)
  })