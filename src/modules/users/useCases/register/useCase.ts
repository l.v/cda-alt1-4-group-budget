import {UseCase} from "../../../../framework/UseCaseI";
import {DTO} from "./dto";

import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export class RegisterUseCase implements UseCase<DTO, any> {
 
  constructor(
    private userRepository,
  ) {
  }

  async execute(request: DTO) : Promise<any> {
    const user = await prisma.user.create({data: {
      username: request.username
    }})

    return user;
  }
}
