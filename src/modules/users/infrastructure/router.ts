import { JoinGroupController } from '../useCases/joinGroup/controller'
import { JoinGroupUseCase } from '../useCases/joinGroup/useCase'
import { RegisterController, RegisterUseCase } from "../useCases/register";

import * as express from 'express'
import { Router } from 'express'


export default function buildRouter(userRepository, groupRepository) {
    const userRouter: Router = Router();


    const joinGroupUseCase = new JoinGroupUseCase(userRepository, groupRepository);
    const joinGroupController = new JoinGroupController(joinGroupUseCase);

    userRouter.post('/join', (req, res) => joinGroupController.execute(req, res) );


    const registerUseCase = new RegisterUseCase(userRepository);
    const registerController = new RegisterController(registerUseCase);

    userRouter.post('/register', (req, res) => registerController.execute(req, res) );

    return userRouter;
};
