export type DTO = {
  userId: number;
  toUserId: number;
  groupId: number;
  amount: number;
}
