import {UseCase} from "../../../../framework/UseCaseI";
import {DTO} from "./dto";

import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export class AddRefundUseCase implements UseCase<DTO, any> {
 
  constructor(
    private userRepository,
    private groupRepository,
  ) {
  }

  async execute(request: DTO) : Promise<any> {
    const update = await prisma.refund.create({
      data: {
        amount: request.amount,
        fromUser: { connect: { id: request.userId, } },
        toUser:   { connect: { id: request.toUserId, } },
        group:    { connect: { id: request.groupId, } },
      },
    });

  }
}
