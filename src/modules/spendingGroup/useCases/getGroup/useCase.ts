import {UseCase} from "../../../../framework/UseCaseI";
import {DTO} from "./dto";

import { Expense, Prisma, PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()


type Debt = {
  toId: number;
  amount: number;
}

const addExpenses = (expenses: {amount: number}[]) => expenses.reduce((acc, curr) => acc + curr.amount, 0);

export class GetGroupUseCase implements UseCase<DTO, any> {
  constructor(
    private groupRepository,
  ) {
  }

  async execute(request: DTO) : Promise<any> {
    const group = await prisma.group.findUnique({
      where: { id: request.groupId },
      include: {
        users: true,
        expenses: true,
      }
    })

    if (!group) {
      return //todo error
    }

    const totalExpenses = addExpenses(group.expenses);
    const averageExpense = Number((totalExpenses / group.users.length).toFixed(2));
 
    const expensesByUser = group.users.map(user => {
      const amountPaid = addExpenses(group.expenses.filter(expense => expense.userId == user.id));
      const toBeRepaid = amountPaid > averageExpense ? amountPaid - averageExpense  : 0;
      const toPay = amountPaid > averageExpense ? 0 : averageExpense - amountPaid;
    
      return {
        id: user.id,
        amountPaid,
        toBeRepaid,
        toPay,
        deficitAfterSettlement: toBeRepaid > 0 ? amountPaid - toBeRepaid : amountPaid + toPay
      }
    })

    //const usersBelowAverage = expensesByUser.filter(user => user.amountPaid < averageExpense);
    //const highestPayer = expensesByUser.reduce((acc, curr) => curr.amountPaid > acc.amountPaid ? acc : curr);

    const usersToBeRepaid = expensesByUser.filter(user => user.toBeRepaid > 0);

    //TODO move that to ../domain/debt
    const settleDebt = (debt: number, credit: number) => {
      if (credit >= debt) {
        return {
          debtRemaining: 0,
          creditRemaining: credit - debt,
          creditUsed: debt
        }
      }

      return {
        debtRemaining: debt - credit,
        creditRemaining: 0,
        creditUsed: credit
      }
    }

    const aggregate = expensesByUser.map(expenseByUser => {
      const usersDebts: Debt[] = [];


        if (expenseByUser.toPay > 0) {
          let amountAvailable = expenseByUser.toPay;
          while (amountAvailable > 0 && usersToBeRepaid.length) {
            const userToRepay = usersToBeRepaid[0];
            const toRepay = userToRepay.toBeRepaid;
            
            //console.log(1, {toRepay, amountAvailable})
            const { debtRemaining, creditRemaining, creditUsed } = settleDebt(toRepay, amountAvailable);
            userToRepay.toBeRepaid = debtRemaining;
            amountAvailable = creditRemaining;

            const usersDebt = {
              toId: userToRepay.id,
              amount: creditUsed
            };

            //console.log(2, { debtRemaining, creditRemaining, creditUsed, usersDebt});

            usersDebts.push(usersDebt)

            if (userToRepay.toBeRepaid === 0) {
              usersToBeRepaid.shift();
            }
          }
        }

        return {
          ...expenseByUser,
          usersDebts
        };

    })

    return aggregate;
  }
}
