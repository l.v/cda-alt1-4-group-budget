import {UseCase} from "../../../../framework/UseCaseI";
import {CreateGroupDTO} from "./dto";
//import { JoinGroupUseCase } from "../joinGroup/useCase";

import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export class CreateGroupUseCase implements UseCase<CreateGroupDTO, any> {
 
  constructor(
    private userRepository,
    private groupRepository,
  ) {
  }

  async execute(request: CreateGroupDTO) : Promise<any> {
    // const user = await prisma.user.findUnique({
    //   where: {id: request.userId}
    // })
    const group = await prisma.group.create({
      data: {
      }
    });

    const updateUser = await prisma.user.update({
      where: {
        id: request.userId,
      },
      data: {
        groups: {
          connect: {
            id: group.id,
          },
        },
      },
    })

    //JoinGroupUseCase({userId: request.userId, groupId: group.id})

  }
}
