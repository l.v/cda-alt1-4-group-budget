import {UseCase} from "../../../../framework/UseCaseI";
import {DTO} from "./dto";

import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export class JoinGroupUseCase implements UseCase<DTO, any> {
 
  constructor(
    private userRepository,
    private groupRepository,
  ) {
  }

  async execute(request: DTO) : Promise<any> {
    const updateUser = await prisma.group.update({
      where: { id: request.groupId },
      data: { isSettling: true },
    })

  }
}
