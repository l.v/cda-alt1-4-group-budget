import {UseCase} from "../../../../framework/UseCaseI";
import {CreateGroupInviteLinkDTO} from "./dto";

import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export class CreateGroupInviteLinkUseCase implements UseCase<CreateGroupInviteLinkDTO, any> {
 
  constructor(
    private groupRepository,
  ) {
  }

  async execute(request: CreateGroupInviteLinkDTO) : Promise<any> {
    const update = await prisma.groupInviteLink.create({
      data: {
        group: {connect: { id: request.groupId }},
        url: "abc"
      }
    });
  }
}
