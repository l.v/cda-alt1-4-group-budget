import {UseCase} from "../../../../framework/UseCaseI";
import {DTO} from "./dto";

import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

export class CancelExpenseUseCase implements UseCase<DTO, any> {
 
  constructor(
    private userRepository,
    private groupRepository,
  ) {
  }

  async execute(request: DTO) : Promise<any> {
    const update = await prisma.expense.update({
      where: { id: request.expenseId },
      data: { cancelled: true },
    });

  }
}
