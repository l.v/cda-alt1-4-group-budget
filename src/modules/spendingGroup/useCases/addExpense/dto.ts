export type DTO = {
  userId: number;
  groupId: number;
  amount: number;
  text: string;
}
