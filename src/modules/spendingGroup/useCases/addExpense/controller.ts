import * as express from 'express'

import { BaseController } from "../../../../framework/ControllerI"

import { AddExpenseUseCase } from "./useCase";
import { DTO } from "./dto";

export default class AddExpenseController extends BaseController {
  constructor(
    private useCase: AddExpenseUseCase
  ) {
    super();
  }

  async  executeImpl ( req: express.Request, res: express.Response ) : Promise<any> {

    const dto = req.body as DTO;
    this.useCase.execute(dto);
  
    return this.ok<any>(res);
  }
}
  